package be.kdg.generics;

import java.util.Iterator;
import java.util.List;

public class MaxUtil {
    // Maak deze methode generiek zodat ze toepasbaar is op een Collection
    // van om het even welk type. Denk eraan dat elk van de types
    // Comparable moet zijn.

    public static String max(List<String> list) {
        Iterator<String> iterator = list.iterator();
        String maximum = iterator.next();
        while (iterator.hasNext()) {
            String x = iterator.next();
            if (maximum.compareTo(x) < 0) {
                maximum = x;
            }
        }
        return maximum;
    }
}

